#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QList>
#include <QTimer>

const int INTERVAL = 500;

enum DIRECTION {UP, DOWN, LEFT, RIGHT};


class game : public QWidget
{
    Q_OBJECT

public:
    game(QWidget* parent = 0);

    void paintEvent(QPaintEvent *e);
    void keyPressEvent(QKeyEvent *e);

    void initialize();

    void generate_apple();

    bool game_over();
    void game_is_over();

private:
    QList<QPoint> snakebody;
    DIRECTION d;
    QPoint apple;
    QTimer *Timer;

private slots:
    void update_snake();

};

#endif // GAME_H
