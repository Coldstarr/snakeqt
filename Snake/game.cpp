#include "game.h"
#include <QPainter>
#include <cstdlib>
#include "time.h"
#include <QMessageBox>

game::game(QWidget* parent): QWidget(parent)
{
    resize(750, 600);
    initialize();
}

void game::initialize(){

    for (size_t i = 0; i < 4; ++i)
    snakebody.push_back(QPoint( 4 - i , 0));

    d = RIGHT;
    apple.setX(375 / 15);
    apple.setY(300 / 15);

    srand(time(0));
    Timer = new QTimer(this);
    connect(Timer,SIGNAL(timeout()),this,SLOT(update_snake()));
    Timer->start(INTERVAL);

    return;
}

void game::paintEvent(QPaintEvent *e){
    QPainter painter(this);

    painter.setBrush(Qt::green);
    painter.drawRect(0, 0, 800, 600);

    painter.setBrush(Qt::red);
    for (int i = 0; i < snakebody.size(); ++i)
    painter.drawRect(snakebody[i].x() * 15,snakebody[i].y() * 15, 15, 15);

    painter.setBrush(Qt::blue);
    painter.drawRect(apple.x() * 15, apple.y() * 15, 15, 15);
    return;
}

void game::keyPressEvent(QKeyEvent *e){
    switch(e->key()){
    case Qt::Key_Up:
        if (d != DOWN){
            d = UP;
        update_snake();
        }
        break;

    case Qt::Key_Down:
        if (d != UP){
            d = DOWN;
        update_snake();
        }
        break;

    case Qt::Key_Left:
        if (d != RIGHT){
            d = LEFT;
        update_snake();
        }
        break;

    case Qt::Key_Right:
        if (d != LEFT){
            d = RIGHT;
        update_snake();
        }
        break;

    default:
        break;
    }
}

//update the snake by push_front and pop_back.
void game::update_snake(){
    switch(d){

    case UP:
    snakebody.push_front(QPoint(snakebody[0].x(), snakebody[0].y()-1));
        break;

    case DOWN:
    snakebody.push_front(QPoint(snakebody[0].x(), snakebody[0].y()+1));
        break;

    case LEFT:
    snakebody.push_front(QPoint(snakebody[0].x()-1, snakebody[0].y()));
        break;

    case RIGHT:
    snakebody.push_front(QPoint(snakebody[0].x()+1, snakebody[0].y()));
        break;

    default:
        break;
    }

    if (snakebody[0] != apple)
        snakebody.pop_back();

    else
        generate_apple();

    if(game_over()){
        game_is_over();
        return;
    }

    repaint();
    return;
}

void game::generate_apple(){
    apple.setX(rand() % 50);
    apple.setY(rand() % 40);
    return;
}

bool game::game_over(){
    int x = snakebody.front().x();
    int y = snakebody.front().y();
    //if the snake bumps the wall
    if (x<0 || y<0 || x>749|| y>599)
        return true;

    //if the snake bumps himself
    for (int i = 2 ; i < snakebody.size(); i++){
        if (snakebody[i].x() == snakebody.front().x() && snakebody[i].y() == snakebody.front().y())
            return true;
    }

    return false;
    }

void game::game_is_over(){
    Timer->stop();
    QMessageBox::information(this, "", "Game over!!");
    initialize();
}
